<?php get_header(); ?>

<main>
	
<section class="under_fv" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/service_fv.png');">
	<div class="under_fv_txtarea">
		<h2 class="under_fv_jp h_mincho">診療内容</h2>
		<p class="under_fv_eng">Services</p>
	</div>
</section>

<section class="pd-common" style="background-color: #eeede6;">
	<div class="container">
        <div class="text-center top_feature_txt mb-xs-50">
        
<p>診察では、いつ頃から・どんな症状が・どれくらいの頻度で起こっているのかなど、<br class="pc">時間の経過にともなう体の変化を詳しくお伺いしています。</p>

<p>研究によって新しく実証されたことや、臨床での結果を患者さま一人ひとりの症状や<br class="pc">社会的背景と合わせ、最適な治療を行っています。</p>
        
        </div>
		<div class="row">
			<!-- 呼吸器科 ここから -->
			<div class="col-sm-12 mt50 mt-xs-20 mb100 mb-xs-60" id="service01">
				<ul class="ul-2 ul-xs-1 service_ul mb-xs-10">
					<li>
						<div class="service_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/service01.jpg');"></div>
					</li>
					<li>
						<div class="service_title_outer">
							<p class="service_title_jp main_color h_mincho">呼吸器科</p>
							<p class="service_title_eng main_color">Respiratory medicine</p>
						</div>
						<div class="text-justify service_txt">

<p>咳や息苦しさなど、呼吸器の疾患に対応します。</p>

<p>採血によるアレルギー検査、呼吸機能検査、睡眠時無呼吸症候群についての検査、禁煙外来を行っています。</p>

<p>「散歩や階段を上がるときに息苦しさを感じた」、「風邪が治っても咳が止まらない」といった小さな不安でも、お気軽にご相談下さい。</p>


                        </div>
					</li>
				</ul>
                
				<div class="service_content_area">
					<p class="service_content_title h_mincho">このような疾患・症状の方へ</p>
					<ul class="service_content_ul">
						<li>かぜの諸症状発熱（のどの痛み・鼻水・頭痛・嘔吐・下痢・咳・たん）</li>
						<li>インフルエンザ</li>
						<li>花粉症</li>
						<li>喘息</li>
						<li>気管支炎</li>
						<li>肺炎</li>
						<li>結核</li>
						<li>息切れ</li>
						<li>息苦しさ</li>
                        <li>COPD（肺気腫）</li>
                        <li>いびき</li>
                        <li>マイコプラズマ</li>
                        <li>血痰、など</li>
                    </ul>
				</div>
			</div>
			<!-- 呼吸器科 ここまで -->
			
			<!-- 内科 ここから -->
			<div class="col-sm-12 mb100 mb-xs-60" id="service02">
				<ul class="ul-2 ul-xs-1 service_ul mb-xs-10">
					<li>
						<div class="service_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/service03.jpg');"></div>
					</li>
					<li>
						<div class="service_title_outer">
							<p class="service_title_jp main_color h_mincho">内科</p>
							<p class="service_title_eng main_color">Internal medicine</p>
						</div>
						<div class="text-justify service_txt">
                        
<p>一般的な疾患から予防医療まで幅広く、全般的に対応します。様々な疾患の可能性を考慮し、問診に加えて肺音や心臓音などの聴診も重視しています。</p>
<p>レントゲンによる呼吸器の検査、心電図による循環器の検査、超音波（エコー）による腹部臓器、泌尿器、循環器の検査も行っています。</p>

<p>健康診断で異常を指摘された方もご相談下さい。</p>
                        
                        </div>
					</li>
				</ul>
				<div class="service_content_area">
					<p class="service_content_title h_mincho">このような疾患・症状の方へ</p>
					<ul class="service_content_ul">

<li>頭痛</li><li>片頭痛</li><li>胸痛</li><li>腹痛</li><li>動悸</li><li>胃潰瘍</li><li>胃炎</li><li> 腸炎</li><li>胸やけ</li><li>胃もたれ</li><li>下痢など</li><li>高血圧</li><li>高脂血症</li><li>肥満症</li><li>糖尿病</li><li>貧血</li><li>痛風</li><li> 逆流性食道炎</li><li>不眠症</li><li>便秘</li><li>ピロリ菌、など</li>        
                    
					</ul>
				</div>
			</div>
			<!-- 内科 ここまで -->
			
			<!-- 泌尿器科 ここから -->
			<div class="col-sm-12 mb100 mb-xs-60" id="service03">
				<ul class="ul-2 ul-xs-1 service_ul mb-xs-10">
					<li>
						<div class="service_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/service02.jpg');"></div>
					</li>
					<li>
						<div class="service_title_outer">
							<p class="service_title_jp main_color h_mincho">泌尿器科</p>
							<p class="service_title_eng main_color">Urology medicine</p>
						</div>
						<div class="text-justify service_txt">

<p>排尿時の痛みや違和感、頻尿、急な尿意など生活の質を低下させる尿のお悩みに対応します。</p>

<p>陰部の痛み・かゆみ・腫れなどの悩みがある方もお気軽にご相談下さい。性病検査も行っています。</p>

                        
                        </div>
					</li>
				</ul>
				<div class="service_content_area">
					<p class="service_content_title h_mincho">このような疾患・症状の方へ</p>
					<ul class="service_content_ul">
                    <li>前立腺肥大症</li><li>膀胱炎</li><li>尿道炎</li><li>過活動膀胱</li><li>尿路結石症</li><li>血尿</li><li>蛋白尿</li><li>前立腺癌</li><li>勃起不全（ED）</li><li>性感染症（淋病・クラミジア・梅毒）、など</li>
                    </ul>
				</div>
			</div>
			<!-- 泌尿器科 ここまで -->
			
			<!-- 皮膚科 ここから -->
			<div class="col-sm-12 mb50" id="service04">
				<ul class="ul-2 ul-xs-1 service_ul mb-xs-10">
					<li>
						<div class="service_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/service04.jpg');"></div>
					</li>
					<li>
						<div class="service_title_outer">
							<p class="service_title_jp main_color h_mincho">皮膚科</p>
							<p class="service_title_eng main_color">Dermatology medicine</p>
						</div>
						<div class="text-justify service_txt">
                        
<p>皮膚のかゆみや痛み、赤みなどの症状に対応します。</p>

<p>皮膚疾患は医療機関でも原因の特定が難しく、季節や住環境など、広く可能性を考慮しながら治療します。</p>

<p>原因がわからない場合でも、お気軽にご相談下さい。</p>

                        </div>
					</li>
				</ul>
				<div class="service_content_area">
					<p class="service_content_title h_mincho">このような疾患・症状の方へ</p>
					<ul class="service_content_ul">


<li>湿疹</li><li>皮膚炎</li><li>虫さされ</li><li>アトピー性皮膚炎</li><li>食物アレルギー</li><li>ヘルペス</li><li>じんましん</li><li>かぶれ</li><li>頭皮のかゆみ</li><li>水虫</li><li>つめ水虫</li><li>乾癬</li><li>ニキビ</li><li>やけど</li><li>魚の目</li><li>痔、など</li>

                    </ul>
				</div>
			</div>
			<!-- 皮膚科 ここまで -->
		</div>
	</div>
</section>
	
<section class="pd-common" style="background-color: #fff;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="service_inspection_maintitle h_mincho mb50">各種検査・検診を承ります。</p>
				<ul class="service_inspection_ul ul-4 ul-xs-2">
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/service_icon10.png" alt="予防接種" class="service_inspection_img">
						<p class="service_inspection_title h_mincho">予防接種</p>
						<p>インフルエンザ・肺炎球菌・帯状疱疹（ヘルペス）・ 2種混合ワクチン・麻しん・風しん　など。定期接種対象の方には公的費用補助の場合があります。</p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/service_icon02.png" alt="血液検査" class="service_inspection_img">
						<p class="service_inspection_title h_mincho">血液検査</p>
						<p>炎症性疾患や臓器の異常、アレルギー検査、生活習慣病などの早期発見と診断、治療の目安に。</p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/service_icon03.png" alt="生活習慣病" class="service_inspection_img">
						<p class="service_inspection_title h_mincho">生活習慣病</p>
						<p>定期的な検査の中で、生活・食事指導をし、症状や健康状態により薬物療法を行います。</p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/service_icon14.png" alt="往診" class="service_inspection_img">
						<p class="service_inspection_title h_mincho">往診</p>
						<p>ご来院が困難な場合に居宅へお伺いする往診を行っております。</p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/service_icon11.png" alt="禁煙外来" class="service_inspection_img">
						<p class="service_inspection_title h_mincho">禁煙外来</p>
						<p>禁煙したいが自信がない方、一度禁煙したがまた吸い始めてしまった方はご相談下さい。</p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/service_icon08.png" alt="舌下免疫治療" class="service_inspection_img">
						<p class="service_inspection_title h_mincho">舌下免疫治療</p>
						<p>スギ花粉症の根本治療。体をスギ花粉に慣らし、花粉症状を改善し薬剤の減量・中止が期待できる治療法で す。</p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/service_icon13.png" alt="睡眠時無呼吸症候群" class="service_inspection_img">
						<p class="service_inspection_title h_mincho">睡眠時無呼吸症候群</p>
						<p>検査機器を使って、ご自宅で睡眠の質を検査することができます。在宅のシーパップで治療します。</p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/service_icon12.png" alt="ED治療" class="service_inspection_img">
						<p class="service_inspection_title h_mincho">ED治療</p>
						<p>プライバシーに配慮して診療を行いますので、安心してご相談ください。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>


<section class="pd-common" style="background-color: #ecebe4;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs"><p class="pt_title_eng">FAQ</p></div>
				<div class="text-center text-center-xs"><h3 class="pt_title_jp h_mincho">よくあるご質問</h3></div>
				
				<ul class="ul-2 ul-sm-1 top_qa_ul">
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 受診の際に必要なものはありますか？</p>
						<p class="top_qa_txt top_qa_txt_a">1.保険証、各種受給者証<br>2.お薬手帳があればご持参下さい。お薬手帳がない場合はこれまで他院で処方されたお薬が分かるものを持ってきてください。<br>3.検診や検査結果などがあればご持参ください。</p>
					</li>
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 体調が悪いのですが、どの科を受診すればいのかわかりません。</p>
						<p class="top_qa_txt top_qa_txt_a">A. どんな症状でも些細な体調の変化でも、不安があれば受診をおすすめします。専門的な治療が必要な場合は、症状や患者さまのご都合に合わせて適切な病院や医院をご紹介いたします。</p>
					</li>
                    <!--
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 子どもは受診できますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. お子さまの診察も行っています。小児科や専門医の診察が必要な場合は、適宜ご紹介いたします。</p>
					</li>
                    -->
                    <!--
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 初診の際に必要なものはありますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. 保険証と、可能であればお薬手帳をご持参下さい。これまでの投薬の記録は、治療の際に役立ちます。</p>
					</li>
                    -->
                    <!--
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 自宅の近くの薬局で薬を受け取ることはできますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. 院外の処方箋も発行しています。ご希望があればお知らせ下さい。</p>
					</li>
                    -->
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. アレルギーの検査はできますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. アレルギーの原因となる物質は非常に多く、すべてを検査することは患者さまにとっても負担が大きくなります。当院では患者さまの症状をみてお話を伺い、可能性のある項目を血液検査によって調べています。</p>
					</li>
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. HIV検査はできますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. 当院では行っていません。</p>
					</li>
                    <!--
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 性病の検査はできますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. 淋病・クラミジア・梅毒について検査を行っています。結果はご本人にのみ、直接お伝えしています。</p>
					</li>
                    -->
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 治療にステロイドは使用しますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. 皮膚科、呼吸器科の治療にはステロイド薬は有用です。当院では脱ステロイド治療は推奨していません。</p>
					</li>
				</ul>
				<script>
					var $ = jQuery;
					$(function(){
						$(".top_qa_txt_q").on("click", function() {
							$(this).next().slideToggle();
						});
					});
				</script>
			</div>
		</div>
	</div>	
</section>


</main>






<?php get_footer(); ?>