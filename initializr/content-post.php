<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>

				<a href="<?php the_permalink();?>">
					<article class="pageNewsArticle tra">
						<div class="row">
                            <style>/*
							<div class="col-sm-4">
							
								<?php if (has_post_thumbnail()):?>
									<?php 
										// アイキャッチ画像のIDを取得
										$thumbnail_id = get_post_thumbnail_id();
										// mediumサイズの画像内容を取得（引数にmediumをセット）
										$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
										$eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
									?>
										<figure class="pageNewsImg bgCenter" style="background-image:url('<?php echo $eye_img_s[0];?>');"></figure>
									<?php else: ?>
								<figure class="pageNewsImg bgCenter" style="background-image:url('<?php echo get_template_directory_uri();?>/img/sample01.png');"></figure>
									<?php endif; ?>
							</div>
                            */</style>
                                <div class="col-sm-12">
								<p class="pageNewsCate cateDate">
									<span class="cate mainColor text_s <?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span><span class="date subColor questrial italic text_s"><?php the_time('y/m/d'); ?></span>
								</p>
								<h3 class="h4 bold pageNewsArticleTitle"><?php the_title();?></h3>
								<p class="text_s excerpt"><?php echo get_the_excerpt(); ?></p>
							</div>
						</div>
					</article>
				</a>



