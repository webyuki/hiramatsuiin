<?php get_header(); ?>
<main>
	
<section class="relative topFvSection">
	<div class="topFv">
		<div class="main_imgBox" data-aos="fade-right">
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv01.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv03.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv04.jpg');"></div>
		</div>
	</div>
	<div class="topFvBoxWrap text-center" data-aos="fade-left">
		<div class="topFvBoxText">
			<p class="h_mincho mb0">患者さまにとって<br>「最善の治療」を目指して</p>
		</div>
	</div>
</section>


<section class="" style="background-color: #ecebe4;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="service_inspection_maintitle h_mincho mb30 mb-xs-30">診療時間</h3>
				<table class="table table-bordered calendar text-center mb50">
					<tbody>
						<tr>
							<th>診療時間</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日</th>
							<th>祝</th>
						</tr>
						<tr>
							<td class="time">9:00〜13:00</td>
							<td>◯</td>
							<td>ー</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>ー</td>
							<td>ー</td>
						</tr>
						<tr>
							<td class="time">16:00〜18:00</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>ー</td>
							<td>ー</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<section class="pd-common">
    <div class="container">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="topAlertBox">
                    <h3 class="service_inspection_maintitle h_mincho mb30 mb-xs-30">受診に際してのお願い・注意事項</h3>
                    <div class="mb30">
                        <h4 class="h_mincho subTitle mb10">発熱時の来院について</h4>
                        <p>発熱などの症状があって受診を予定されている場合、来院前に37.5度が3日以上続く時は事前にお電話でご相談ください。場合によっては院外でお待ちいただくこともありますのでご了承下さい。</p>
                        
                    </div>
                    <div class="mb30">
                        <h4 class="h_mincho subTitle mb10">インフルエンザワクチンの接種について</h4>
                        <p>当分の間、インフルエンザワクチン接種の受付を中止します。</p>
                        
                    </div>
                    <!--<p class="bold">GW中の休診日は、4月29日、5月3日、4日、5日、6日です。</p>
                    <p class="bold">8月の休診日は10日（月）・14日（金）・15日（土）です。<br>毎週日曜日と、火曜日午前中は休診です。</p>-->
                    <!--
                    <p class="bold h3">8月お休みのお知らせ</p>
                    <ul class="topNewsHolidayUl">
                    
                        <li>2日（日）休診</li>
                        <li>4日（火）午前休診</li>
                        <li>9日（日）休診</li>
                        <li>10日（月）休診</li>
                        <li>11日（火）午前休診</li>
                        <li>14日（金）休診</li>
                        <li>15日（土）休診</li>
                        <li>16日（日）休診</li>
                        <li>18日（火）午前休診</li>
                        <li>23日（日）休診</li>
                        <li>25日（火）午前休診</li>
                        <li>30日（日）休診</li>
                    
                    
                    </ul>
                    -->
                </div>
            </div>
        </div>
    </div>
</section>


<section class="pd-common service_content top_greet_content" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_greet.png'); background-color: #ccdfcc;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 service_ul">
					<li>
						<img class="visible-xs mb-xs-20" src="<?php echo get_template_directory_uri(); ?>/img/top_greet.png" alt="誠実な医療をモットーに">
					</li>
					<li>
						<p class="pt_title_eng">Greeting</p>
						<h3 class="pt_title_jp h_mincho">患者さまと向き合い、<br class="sp">心の通う診察を</h3>
						<div class="text-justify mb50 mb-sm-20 mb-xs-30">

<p>平松医院は90年に渡り、岡山市の中心部で開業してまいりました。</p>

<p>最新の研究によって得られた知識と実証をもとに、病気の早期発見・早期治療、予防医療に努め、ていねいな診察と説明で、患者さまにとっての最善の治療に向き合っています。</p>

<p>頼れる地域の「かかりつけ医」として、職場や外出先での「困った時の気軽な相談窓口」として、これからも皆さまの健康に貢献してまいります。</p>
                        
                        </div>
						<div class="text-center-xs"><a href="<?php echo home_url(); ?>/about-us" class="pt_btn01 h_mincho">平松医院について</a></div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common" style="background-color: #ecebe4;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs"><p class="pt_title_eng">Feature</p></div>
				<div class="text-center text-center-xs"><h3 class="pt_title_jp h_mincho">当院の特徴</h3></div>
				<p class="text-center top_feature_txt mb100 mb-xs-50">「平松医院」は90年余り市内中心部にて開業しています。現在でも親子３代にわたって通院してくださる患者さまも多く、何でも相談できる町医者として親しまれて参りました。伝統と信頼を重んじ、引き続き地域の皆さまの健康に貢献して参ります。</p>
				
				<ul class="ul-4 ul-xs-1 top_feature_ul mb20 mb-xs-0">
					<li>
						<div class="top_feature_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_feature01.jpg');"></div>
						<div class="top_feature_txtarea">
							<h4 class="top_feature_title h_mincho">院内処方</h4>
							<p class="top_feature_txt text-justify">お薬を院内の薬局で調剤するため、医院から直接お持ち帰りいただくことができます。特殊なお薬や院外処方箋、遠方の患者さまにも柔軟に対応しています。お気軽にご相談下さい。</p>
						</div>
					</li>
					<li>
						<div class="top_feature_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_feature02.jpg');"></div>
						<div class="top_feature_txtarea">
							<h4 class="top_feature_title h_mincho">ジェネリック医薬品</h4>
							<p class="top_feature_txt text-justify">当院では、患者さまの経済的負担が軽減されるジェネリック医薬品をご用意しています。効きめと安全性を考慮し、先発品と比較しながらより患者さまに合うお薬を処方いたします。ご不明な点はお気軽におたずね下さい。</p>
						</div>
					</li>
					<li>
						<div class="top_feature_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_feature04.jpg');"></div>
						<div class="top_feature_txtarea">
							<h4 class="top_feature_title h_mincho">多岐に渡る診療分野</h4>
							<p class="top_feature_txt text-justify">当院は呼吸器科、内科、皮膚科、泌尿器科と幅広い診療科を設けています。そのため、喘息や花粉症、頭痛、腹痛や膀胱炎、アレルギーまで当院にて診療することが可能です。</p>
						</div>
					</li>
					<li>
						<div class="top_feature_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_feature03.jpg');"></div>
						<div class="top_feature_txtarea">
							<h4 class="top_feature_title h_mincho">病診連携</h4>
							<p class="top_feature_txt text-justify">患者さまの症状により、各地の病院・医院と連携して症状の改善、健康状態の回復をサポートします。ご自宅や職場の近くの病院や医院を紹介することもできますので、詳細についてはお問い合わせください。</p>
						</div>
					</li>
				</ul>
				<div class="text-center text-center-xs mb30"><a href="<?php echo home_url(); ?>/feature" class="pt_btn01 h_mincho">当院の特徴</a></div>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs"><p class="pt_title_eng">Services</p></div>
				<div class="text-center text-center-xs"><h3 class="pt_title_jp h_mincho">診療内容</h3></div>
			</div>
		</div>
	</div>
	<ul class="ul-4 ul-sm-2 ul-xs-1 top_service_ul mb50">
		<li>
			<a href="<?php echo home_url(); ?>/service#service01">
				<div class="top_service_img_outer">
					<div class="top_service_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service01.jpg');"></div>
				</div>
				<div class="top_service_txtarea">
					<h4 class="top_service_jp text-center main_color h_mincho">呼吸器科</h4>
					<p class="top_service_eng text-center main_color">Respiratory medicine</p>
					<p class="text-justify">かぜの諸症状発熱（のどの痛み・鼻水・頭痛・嘔吐・下痢・咳・たん）・インフルエンザ・花粉症・喘息・気管支炎・肺炎・COPD・結核・息切れ・息苦しさなど呼吸器の疾患や症状に対応します。</p>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service#service02">
				<div class="top_service_img_outer">
					<div class="top_service_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service03.jpg');"></div>
				</div>
				<div class="top_service_txtarea">
					<h4 class="top_service_jp text-center main_color h_mincho">内科</h4>
					<p class="top_service_eng text-center main_color">Internal medicine</p>
					<p class="text-justify">頭痛・片頭痛・胸痛・腹痛・動悸・胃潰瘍・胃炎・ 腸炎・胸やけ・胃もたれ・下痢、 慢性疾患： 高血圧・高脂血症・肥満症・糖尿病・貧血・痛風・ 逆流性食道炎・不眠症・便秘・ピロリ菌・痔などの疾患に対応します。</p>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service#service03">
				<div class="top_service_img_outer">
					<div class="top_service_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service02.jpg');"></div>
				</div>
				<div class="top_service_txtarea">
					<h4 class="top_service_jp text-center main_color h_mincho">泌尿器科</h4>
					<p class="top_service_eng text-center main_color">Urology medicine</p>
					<p class="text-justify">前立腺肥大症・膀胱炎・尿道炎・過活動膀胱・尿路結石症・血尿・蛋白尿・前立腺癌・性感染症・勃起不全（ED）、排尿時の痛みや違和感・頻尿・急な尿意など生活の質を低下させる尿のお悩みに対応します。</p>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service#service04">
				<div class="top_service_img_outer">
					<div class="top_service_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service04.jpg');"></div>
				</div>
				<div class="top_service_txtarea">
					<h4 class="top_service_jp text-center main_color h_mincho">皮膚科</h4>
					<p class="top_service_eng text-center main_color">Dermatology medicine</p>
					<p class="text-justify">湿疹・皮膚炎・虫さされ・アトピー性皮膚炎・食物アレルギー・ヘルペス・じんましん・かぶれ・頭皮のかゆみ・水虫・つめ水虫・乾癬・ニキビ・やけど・魚の目など、皮膚のかゆみや痛み、赤みといった症状に対応します。
</p>
				</div>
			</a>
		</li>
	</ul>
	<div class="text-center text-center-xs"><a href="<?php echo home_url(); ?>/service" class="pt_btn01 h_mincho">診療内容</a></div>
</section>

	
<section class="pd-common" style="background-color: #ecebe4;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs"><p class="pt_title_eng">News</p></div>
				<div class="text-center text-center-xs"><h3 class="pt_title_jp h_mincho">新着情報</h3></div>
			</div>
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 top_news_ul">
					<li>
						<h4 class="top_news_title h_mincho">お知らせ</h4>
						<table class="top_news_table mb50 mb-xs-20">
							<tbody>
					<?php
						//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
						$paged = get_query_var('page');
						$args = array(
							'post_type' =>  'post', // 投稿タイプを指定
							'paged' => $paged,
							'posts_per_page' => 8, // 表示するページ数
							'orderby'=>'date',
							'cat' => -4,
							'order'=>'DESC'
									);
						$wp_query = new WP_Query( $args ); // クエリの指定 	
						while ( $wp_query->have_posts() ) : $wp_query->the_post();
							//ここに表示するタイトルやコンテンツなどを指定 
						get_template_part('content-post-top'); 
						endwhile;
						//wp_reset_postdata(); //忘れずにリセットする必要がある
						wp_reset_query();
					?>		
							</tbody>
						</table>
						<div class="text-center text-center-xs mb-xs-50"><a href="<?php echo home_url(); ?>/news" class="pt_btn01 h_mincho">お知らせ</a></div>
					</li>
					<li>
						<h4 class="top_news_title h_mincho">お役立ち情報</h4>
						<table class="top_news_table mb50 mb-xs-20">
							<tbody>
					<?php
						//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
						$paged = get_query_var('page');
						$args = array(
							'post_type' =>  'post', // 投稿タイプを指定
							'paged' => $paged,
							'posts_per_page' => 3, // 表示するページ数
							'orderby'=>'date',
							'cat' => 4,
							'order'=>'DESC'
									);
						$wp_query = new WP_Query( $args ); // クエリの指定 	
						while ( $wp_query->have_posts() ) : $wp_query->the_post();
							//ここに表示するタイトルやコンテンツなどを指定 
						get_template_part('content-post-top'); 
						endwhile;
						//wp_reset_postdata(); //忘れずにリセットする必要がある
						wp_reset_query();
					?>		
							</tbody>
						</table>
						<div class="text-center text-center-xs"><a href="<?php echo home_url(); ?>/category/knowledge" class="pt_btn01 h_mincho">お役立ち情報</a></div>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>

</main>



<?php get_footer(); ?>