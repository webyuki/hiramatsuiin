<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>

							<li>
								<a href="<?php the_permalink();?>" class="clearfix">
                                
                                    <?php if (has_post_thumbnail()): ?>
                                    <?php 
                                        // アイキャッチ画像のIDを取得
                                        $thumbnail_id = get_post_thumbnail_id(); 
                                        // mediumサイズの画像内容を取得（引数にmediumをセット）
                                        $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
                                        $eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
                                    ?>
                                        <div class="top_column_img bg-common" style="background-image: url('<?php echo $eye_img_s[0];?>');"></div>
                                    <?php else: ?>
                                        <div class="top_column_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/sample01.png');"></div>
                                    <?php endif; ?>	

									<p class="top_column_txt"><?php the_title();?></p>
									<p class="top_column_date"><?php the_time('Y.m.d'); ?></p>
								</a>
							</li>
