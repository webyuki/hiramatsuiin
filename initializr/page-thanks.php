<?php get_header(); ?>

<main>
<section class="under_fv" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/service_fv.png');">
	<div class="under_fv_txtarea">
		<h2 class="under_fv_jp h_mincho">送信完了</h2>
		<p class="under_fv_eng">Thanks</p>
	</div>
</section>




<section class="pd-common relative paperBgUnder" style="background-color: #eeede6;">
	<div class="container">
		<div class="row">
			<div class="contInCont" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>お問い合わせの送信が完了しました。</p>
					<p>担当者から折り返させてご連絡させて頂きますので、今しばらくお待ち下さい。</p>
				</div>
				<a class="telLink fontEn h0 text-center bold mainColor block mb30" href="tel:0862321070">086-232-1070</a>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="68"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>