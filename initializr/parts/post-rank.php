<div class="post-rank">

	<?php
		// カテゴリ別の条件分岐
		if (in_category('fashion') || post_is_in_descendant_category( get_term_by( 'slug', 'fashion', 'category' ) )) { ?>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/back-pack-cospa/">メンズにオススメな格安リュック・バックパック10選【通勤/通学/1万円以下】</a></p>
		<?php } else if(in_category('hp') || post_is_in_descendant_category( get_term_by( 'slug', 'hp', 'category' ) )){ ?>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/all-free/">全くの未経験から独学でwebデザイナーに転職する為の方法【完全版】</a></p>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/seo-pascal/">断言する！最高のseo内部対策ツール、オロパス社のパスカルがやばいぐらい使える</a></p>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/online-school/">オンラインで受講可能なIT・プログラミングスクールのオススメ4選</a></p>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/rental-server/">webデザイン会社で働く僕が選ぶコスパ最強のレンタルサーバー3選</a></p>
		<?php } else if(in_category('lifehack') || post_is_in_descendant_category( get_term_by( 'slug', 'lifehack', 'category' ) )){ ?>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/wrike-project-tool/">プロジェクト管理ツールwrikeを1ヶ月試してみた結果、最高のツールであることが判明</a></p>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/jisui-books/">本や漫画を電子化・自炊して本棚をスッキリ！2日間で100冊を自炊した方法と手順を公開</a></p>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/paperless-ipad/">ipad proとapple pencilを使って完全ペーパーレス化に成功したやり方とアプリを紹介</a></p>
		<?php } else if(in_category('culture') || post_is_in_descendant_category( get_term_by( 'slug', 'culture', 'category' ) )){ ?>
			<p><span>人気記事</span><a href="<?php echo home_url();?>/applemusic-merit/">1年以上Apple Musicを使い倒した感想・メリットデメリットを紹介
</a></p>
	<?php } ?>

</div>


<?php
	// カテゴリ別の条件分岐
	if (in_category('fashion') || post_is_in_descendant_category( get_term_by( 'slug', 'fashion', 'category' ) )) { ?>
		<?php echo do_shortcode('[fashion]'); ?>

<?php } ?>




<style>
.post-rank span{
	background: #ea4351;
	color: #fff;
	padding: 0.8%;
	margin-right: 1%;
	font-size: 14px;
}
</style>
