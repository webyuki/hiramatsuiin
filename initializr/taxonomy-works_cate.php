<?php get_header(); ?>
<main>


<section class="pageHeader bgMainColor mb100">
	<div class="bgImg bgCircle paddingW imgNone" style="background-image:url('<?php echo get_template_directory_uri();?>/img/bg_circle.png')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="white mb30">
						<p class="fontEn h2">Works</p>
						<h3 class="h3">施工事例</h3>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>


<section class="pageWorkLi margin">
	<div class="container">
		<div class="pageWorksLiLiNavi mb30">
			<ul class="inline_block mainColor">
			
			
				<li><a href="<?php echo home_url();?>/works">全て</a></li>
				<?php $categories = get_categories(array('taxonomy' => 'works_cate')); if ( $categories ) : ?>
					<?php foreach ( $categories as $category ): ?>
						<li><a href="<?php echo home_url();?>/works_cate/<?php echo esc_html( $category->slug);?>"><?php echo wp_specialchars( $category->name ); ?></a></li>
					<?php endforeach; ?>
				<?php endif; ?>
								
			</ul>
		</div>
		<ul class="pageWorkLiUl inline_block">
		
			<?php			
				while ( have_posts() ) : the_post();
					get_template_part('content-post-works-archive'); 
				endwhile;
			?>
		
		</ul>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>










</main>
<?php get_footer(); ?>