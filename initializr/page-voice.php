<?php get_header(); ?>



<section class="area_single">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="entry">
					<?php get_template_part( 'parts/breadcrumb' ); ?>				
					<div class="title_bg title_margin">
						<h2 class="h2 title_main  bold">お客様の声</h2>
					</div>
					<div class="wrapper_thumb">
						<ul>
							<?php
								$paged = (int)get_query_var('paged');
								$args = array(
									'post_type' =>  'example', // 投稿タイプを指定
									'posts_per_page' => 10, // 表示するページ数
									'order'=>'ASC',
									'orderby'=>'menu_order',
									'paged' => $paged
									
								);
								$wp_query = new WP_Query( $args ); // クエリの指定 	
								while ( $wp_query->have_posts() ) : $wp_query->the_post();
									//ここに表示するタイトルやコンテンツなどを指定 
								get_template_part('content','voice'); 						
								endwhile;
								wp_reset_postdata(); //忘れずにリセットする必要がある
							?>		
						</ul>
					</div>
				 <?php get_template_part( 'parts/pagenation' ); ?>
				</div>
			</div>
			<div class="col-sm-3">
				<?php //get_sidebar(); ?>
                <?php get_sidebar(voice); ?>
   			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>






