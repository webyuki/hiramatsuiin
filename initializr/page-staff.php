<?php get_header(); ?>

<main>

<section class="relative topStaff">
	<div class="container">
		<div class="bgImg pageFvImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_staff_fv.jpg');" data-aos="fade-right"></div>
	<div class="pageFvBoxWrap topFvBoxWrap text-center" data-aos="fade-left">
		<div class="topFvBoxText">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Staff</p>
				<h2 class="h3 mb10">スタッフ紹介</h2>
			</div>
		</div>
	</div>
	</div>
</section>

<section class="topService footerLink margin">
	<div class="container">
		<div class="flex justBetween flexWrapSp" data-aos="fade-right">
			<div class="topServiceBox relative">
				<a href="<?php echo home_url();?>/staff/#01">
					<div class="topServiceBoxImg bgImg tra" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_staff_03_01.jpg')"></div>
					<div class="topServiceBoxText tra">
						<p class="fontEn text_m mb0">営業</p>
						<h4 class="h3 bold mainColor titleBd">川原 涼</h4>
					</div>
				</a>
			</div>
			<div class="topServiceBox relative">
				<a href="<?php echo home_url();?>/staff/#02">
					<div class="topServiceBoxImg bgImg tra" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_staff_02_01.jpg')"></div>
					<div class="topServiceBoxText tra">
						<p class="fontEn text_m mb0">経営企画推進室ほか</p>
						<h4 class="h3 bold mainColor titleBd">矢葺 里美</h4>
					</div>
				</a>
			</div>
			<div class="topServiceBox relative">
				<a href="<?php echo home_url();?>/staff/#03">
					<div class="topServiceBoxImg bgImg tra" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_staff_01_01.jpg')"></div>
					<div class="topServiceBoxText tra">
						<p class="fontEn text_m mb0">部長</p>
						<h4 class="h3 bold mainColor titleBd">西谷 悠</h4>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="margin" id="01">
	<div class="container">
		<div class="padding paddingInner bgWhite">
			<div class="container">
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_03_01.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 bold mb10 titleBd">お客さまを守る知識を、ひとつでも多く身につけたい</h4>
						<p class=""><span class="h3 bold mainColor">川原 涼</span><span class="staffRoleText text_m">営業</span></p>
						<p class="text_m gray mb30">2015年　新卒採用</p>
						<p class="">お客さまの「今」にふさわしい商品のご提案や、満期を迎えた保険についてのご相談など、これまでかけてこられた保険のメンテナンスを中心に、個人や法人のお客さまにサービスをご提供しています。</p>
						<p>日々の業務のなかで、自分の知識がお客さまの大切な命や財産を守るものとなることを実感し、これまで以上に誰かの役に立てる人になりたいと感じています。</p>
					</div>
				</div>
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_03_03.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="h5 bold gray">Q1.この仕事の魅力は？</p>
						<h4 class="h3 mb30 titleBd">自分の知識がお客さまを守る力になる</h4>

<p>この仕事をはじめてから、自分の知識がお客さまの日々の生活やご家族、財産を守る力になることを実感し、魅力に感じるようになりました。</p>
<p>保険のことだけでなく、ご家族やお仕事など、様々なお話を通してお客さまのことをより深く知り、お役に立てる機会を増やしていきたいと思っています。</p>

					</div>
				</div>
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
						<!--<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_01_04.jpg" alt="">-->
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="h5 bold gray">Q2.今後の目標は？</p>
						<h4 class="h3 mb30 titleBd">お客さま同士のネットワークを広げたい</h4>

<p>私たち菊池保険サービスが培ってきたつながりをいかし、お客さま同士のネットワークを広げていけたらと思います。</p>
<p>倉敷を中心とした地元のお客さまに寄り添い、安心や生きがいを守り、地域に貢献する活動を支えるといった、人と人、人と企業とのつながりを作っていける存在になりたいと考えています。</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="margin" id="02">
	<div class="container">
		<div class="padding paddingInner bgWhite">
			<div class="container">
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_02_01.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 bold mb10 titleBd">個々のニーズに合うサービスをご提案し、「もしも」の支えを作りたい</h4>
						<p class=""><span class="h3 bold mainColor">矢葺 里美</span><span class="staffRoleText text_m">経営企画推進室ほか</span></p>
						<p class="text_m gray mb30">2017年　中途採用</p>

<p>この会社では事務や営業など、これまでの経験をフルに活用し、お客さまご自身やご家族との人生を保険会社という立場から、力強くサポートができることを何よりもうれしく感じています。</p>
<p>バックオフィスをメインに担当していますが、営業としてお客さまとお会いする時間も大切にしています。保険についてのお悩みは、いつでもお気軽にご相談ください。</p>

					</div>
				</div>
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_02_03.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="h5 bold gray">Q1.この仕事の魅力は？</p>
						<h4 class="h3 mb30 titleBd">お客さまを守るサービスを、自分の手で作れる幸せ</h4>

<p>この仕事について以来、それまで実態をよく知らなかった保険が、いかにお客さまを助けるものであるかを理解するようになりました。</p>
<p>今は目の前にいるお客さまのニーズに合う商品や選び方をご案内し、ご提供したサービスがお客さまの役に立つ瞬間まで見届けられることが、この仕事の魅力だと感じています。</p>

					</div>
				</div>
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
						<!--<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_01_04.jpg" alt="">-->
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="h5 bold gray">Q2.今後の目標は？</p>
						<h4 class="h3 mb30 titleBd">明日は同じことをしない</h4>

<p>私はこれまでも同じことの単純なくり返しではなく、アイデアや変化を加えながら、「明日は同じことをしない」をモットーに仕事に向き合ってきました。</p>
<p>菊池保険サービスには、新しいことへの挑戦や実現を後押ししてくれる雰囲気があります。その社風を追い風に、より新しいものや、保険業界にとらわれない枠の中で、保険代理店として輝ける働き方を見つけていきたいと考えています。</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="margin" id="03">
	<div class="container">
		<div class="padding paddingInner bgWhite">
			<div class="container">
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_01_01.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 bold mb10 titleBd">保険に対するイメージを変え、お客さまとの絆を深めたい</h4>
						<p class=""><span class="h3 bold mainColor">西谷 悠</span><span class="staffRoleText text_m">部長</span></p>
						<p class="text_m gray mb30">2015年　中途採用</p>

<p>人と保険をダイレクトに結べる仕事を希望し、菊池保険サービスに入社して4年。従来の「わかりにくい」保険のイメージを変えるため、お客さま目線に立ったサービスを提供してまいりました。</p>
<p>「相談しやすさ・わかりやすさ」を大切にしたおつきあいの中で、お客さま一人ひとりのライフスタイルにあった保険をご提案しています。</p>


					</div>
				</div>
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_01_03.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="h5 bold gray">Q1.この仕事の魅力は？</p>
						<h4 class="h3 mb30 titleBd">「君の話で、はじめて保険のことがよくわかったよ」の言葉を励みに</h4>

<p>保険は目に見えない商品でありながら、サービス内容は複雑でわかりにくいという、お客さまにとってプラスとは言えない性質を持っています。</p>
<p>だからこそその魅力や、保険がお客さまの現在や未来を守るものであることを適切にお伝えできれば、自分がお客さまの「もしも」の一助になれると感じられるところが魅力です。</p>

					</div>
				</div>
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
						<!--<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_01_04.jpg" alt="">-->
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="h5 bold gray">Q2.やりがいを感じるのはどんなとき？</p>
						<h4 class="h3 mb30 titleBd">ご紹介はお客さまからの信頼の証</h4>

<p>長くおつきあいさせていただいたお客さまが、ご家族やご友人など、大切な方を紹介してくださったときです。</p>
<p>費用がかかり、内容も理解しにくいサービスでありながら、それでも大切な人をご紹介してくださるということは、私や菊池保険サービスを信頼していただけたという、何よりの証。一生懸命やってきてよかったと思う瞬間です。</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>