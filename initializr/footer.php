<section class="pd-common f_area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-4 ul-xs-1 top_feature_ul mb50 mb-sm-20 mb-xs-0">
					<li>
						<a href="<?php echo home_url(); ?>/about-us">
							<div class="top_feature_img_outer">
								<div class="top_feature_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/f_img01.jpg');"></div>
							</div>
							<div class="top_feature_txtarea">
								<h4 class="top_feature_title h_mincho">ご挨拶</h4>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/service">
							<div class="top_feature_img_outer">
								<div class="top_feature_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/f_img02.jpg');"></div>
							</div>
							<div class="top_feature_txtarea">
								<h4 class="top_feature_title h_mincho">診療内容</h4>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/feature">
							<div class="top_feature_img_outer">
								<div class="top_feature_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/f_img03.jpg');"></div>
							</div>
							<div class="top_feature_txtarea">
								<h4 class="top_feature_title h_mincho">当院の特徴</h4>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/access">
							<div class="top_feature_img_outer">
								<div class="top_feature_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/f_img04.jpg');"></div>
							</div>
							<div class="top_feature_txtarea">
								<h4 class="top_feature_title h_mincho">アクセス・診療時間</h4>
							</div>
						</a>
					</li>
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="ul-2 ul-xs-1 f_info_ul">
					<li>
						<p class="f_logo_subtitle">岡山市北区で呼吸器科なら平松医院</p>
						<a class="f_logo" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
					</li>
					<li>
						<p class="mb0">〒700-0826<br>岡山県岡山市北区磨屋町9-5<br>(医院裏手に駐車場2台あり)</p>
					<!--	<p class="mb0">FAX：086-232-1070</p>-->
						<!--<a href="tel:0862321070"><p class="f_tel mb30">086-232-1070</p></a>-->
						<a href="tel:0862321070"><p class="f_tel mb30">086-232-1070</p></a>
					</li>
				</ul>
				<table class="table table-bordered calendar text-center mb50 mb-sm-30 mb-xs-30">
					<tbody>
						<tr>
							<th>診療時間</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日</th>
							<th>祝</th>
						</tr>
						<tr>
							<td class="time">9:00〜13:00</td>
							<td>◯</td>
							<td>ー</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>ー</td>
							<td>ー</td>
						</tr>
						<tr>
							<td class="time">16:00〜18:00</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>ー</td>
							<td>ー</td>
						</tr>
					</tbody>
				</table>
				<div class="text-center text-center-xs mb-sm-50 mb-xs-50"><a href="<?php echo home_url(); ?>/contact" class="pt_btn01 h_mincho">お問い合わせ</a></div>
			</div>
			<div class="col-md-6">
				<iframe class="mb50" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3281.6815126641554!2d133.92257991553979!3d34.662745380444306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x35540635862da95f%3A0x901c044d11842e94!2z44CSNzAwLTA4MjYg5bKh5bGx55yM5bKh5bGx5biC5YyX5Yy656Oo5bGL55S677yZ4oiS77yV!5e0!3m2!1sja!2sjp!4v1568292904759!5m2!1sja!2sjp" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				
				<div class="text-center text-center-xs mb-xs-50"><a href="<?php echo home_url(); ?>/access" class="pt_btn01 h_mincho">詳しいアクセスはこちら</a></div>
			</div>
		</div>
	</div>
</section>

<script>
	$('.top_pro_slider').slick({
	  slidesToShow: 4,
	  centerMode: true,
	  arrows: false,
	  autoplay: true,
	  autoplaySpeed: 0, //待ち時間を０に
	  speed: 8000, // スピードをゆっくり
	  swipe: false, // 操作による切り替えはさせない
	  cssEase: 'linear', // 切り替えイージングを'linear'に
	  // 以下、操作後に止まってしまう仕様の対策
	  pauseOnFocus: false,
	  pauseOnHover: false,
	  pauseOnDotsHover: false,

	  // 以下、レスポンシブ
	  responsive: [
		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 2,
		  }
		}
	  ]
	});
</script>

<?php wp_footer(); ?>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement	: "bottom-top",
	duration: 1000,
});
</script>
</body>
</html>
 