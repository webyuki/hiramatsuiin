<?php get_header(); ?>

<main>

<section class="relative topStaff">
	<div class="container">
		<div class="bgImg pageFvImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_recruit_fv.jpg');" data-aos="fade-right"></div>
	<div class="pageFvBoxWrap topFvBoxWrap text-center" data-aos="fade-left">
		<div class="topFvBoxText">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Recruit</p>
				<h2 class="h3 mb10">採用情報</h2>
			</div>
		</div>
	</div>
	</div>
</section>

<section class="pageRecruitMessage margin">
	<div class="container">
		<div class="text-center">
			<p class="fontEn h3 titleLine mb10">Message</p>
			<h3 class="h3 bold mb30 mainColor">代表メッセージ</h3>
		</div>
		<div class="width720" data-aos="fade-up">
			<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_recruit_01.jpg" alt="">
		</div>
		<div class="width720" data-aos="fade-up">

<p>2018年夏に西日本を襲った豪雨によって、私たちは職場や自宅、それらのあったまちを失いました。そして私たちのお客さまもまた、大切なものを失いました。</p>
<p>多くのものを失くした一方で、この災害によって私たちが学んだことは少なくありませんでした。</p>
<p>危機に直面したときにこそ自ら考えて行動し、改善を重ねながら物事をよい方向へ進めていく力がどれほど大切であるかを、身をもって知るきっかけともなりました。</p>
<p class="mb30">これから私たちの仲間になってくれる方には、想いを共有し、大切なお客さまに向き合い、ともに最善を追究していける方であることを願います。</p>
<p>複雑に見える多種多様な保険商品は、深めるほど自らの財産となり、お客さまの安心を守る力となります。</p>
<p>社員のがんばりには、会社として全力で応えていく準備もあります。</p>
<p class="mb30">互いを補う力を持った先輩社員と力を合わせ、ときに切磋琢磨し、弊社だけでなく、業界全体に新しい風を吹き込んでくれる人材に出会えることを、私たちは心から楽しみにしています。</p>
			<p class="text_m mb0">代表取締役</p>
			<p class="h3">菊池 達也</p>
		</div>

	</div>
</section>

<section class="topService margin">
	<div class="container">
		<div class="padding bgWhite">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Wanted</p>
				<h3 class="h3 bold mb30 mainColor">求める人物像</h3>
			</div>
			<div class="width720 mb50">
				<p>私たちは、一にお客様に喜んでいただく経営。二に人の役に立った実感を持てる人になること。三に社員が心豊かな暮らしをすること。この三つの理念を大切にしています。 自分だけが喜ぶのではなく、どうすれば自分の周りの人にも喜んでもらえるのかを日々考え、行動していきたいと考えています。
</p>
			</div>
			<div class="container">
				<div class="row mb30" data-aos="fade-up">
					<div class="col-sm-4">
						<div class="topAboutBox">
							<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_recruit_ico_01.png" alt="">
							<h4 class="h4 mb10 titleBd text-center">不安を和らげられる</h4>
							<p class="gray text_m">保険が必要とされるのは、お客さまが危機に直面したとき。すばやい対応で、お客さまの不安を和らげられる人材を求めています。</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="topAboutBox">
							<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_recruit_ico_02.png" alt="">
							<h4 class="h4 mb10 titleBd text-center">前向きにチャレンジできる</h4>
							<p class="gray text_m">弊社では新しいことに前向きにチャレンジしながら、常にお客さまにとって最善のサービスとは何かを考えられる人材を求めています。</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="topAboutBox">
							<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_recruit_ico_03.png" alt="">
							<h4 class="h4 mb10 titleBd text-center">自ら考えて成長できる</h4>
							<p class="gray text_m">得意なことを最大限にいかし、仲間と補い合いながら、自ら考えて成長していける人材を求めています。</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="margin">
	<div class="container">
		<div class="padding paddingInner bgWhite">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Works</p>
				<h3 class="h3 bold mb30 mainColor">仕事内容</h3>
			</div>
			<div class="width720 mb50">

<p>私たちは個人と企業のお客さまを対象に、現在加入されている保険が適切であるか、「もしも」の時に必要な補償をカバーする内容であるかを検証し、最適なサービスをご案内しています。</p>
<p>お客さまとそのご家族、企業と社員さまなど、関わる方とのコミュニケーションを深め、安心して暮らし、働ける人生の基盤づくりに貢献します。</p>

			</div>
			<div class="container">
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/top_top_service_01.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 mb30 titleBd">個人向け型保険</h4>
<p>個人のお客さまにむけて、生命保険や火災保険、自動車保険など、日々の暮らしのなかで起こる「もしも」を支える保険サービスをご提供しています。</p>
<p>「守る」保険の整ったお客さまには、学資保険といった貯蓄型の保険や、資産運用などについてのご相談も承っています。</p>


					</div>
				</div>
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/top_top_service_02.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 mb30 titleBd">法人向け型保険</h4>

<p>倉敷・総社・岡山地域を中心とした地元の法人企業のお客さまを対象に、ケガや病気などの医療、車両や火災など、損害に関わる保険サービスその他をご提供しています。</p>
<p>企業が抱える大きなリスクから会社と社員の皆さま、関わる方々を守ります。年金や退職金などについてのご相談も承っています。</p>

					</div>
				</div>
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/top_top_service_03.jpg" alt="">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 mb30 titleBd">企業保険管理サービス</h4>

<p>企業のお客さまを対象に加入済保険の分析と見直しを行い、現在の状況に合わせた最適なプランをご提案します。</p>
<p>経営理念・リスクマネジメント・収益改善など、様々な方向から公正な判断を行い、管理代行など適正なマネジメントと企業様の業務軽減を進めます。</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin">
	<div class="container">
		<div class="padding paddingInner bgWhite">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Detail</p>
				<h3 class="h3 bold mb30 mainColor">募集要項</h3>
			</div>
			
			<div class="detail1 mb80">
				<div class="width720">
					<h4 class="h4 mb10 titleBd text-center inlineBock mb30">法人営業スタッフ(営業経験者歓迎)</h4>
					<div class="mb30">
					
	<p>倉敷市、総社市、岡山市を中心に法人のお客様に対する営業スタッフの募集です。</p>
	<p>当社の営業スタイルは「保険の押し売り」ではなく、保険サービスを通して、お客様の経営パートナーとなることを目指しています。</p>
	<p>当社には保険加入状況を「経営者の想いの実現」「リスクマネジメント」「収益改善」などの観点から分析、診断できる独自サービス、「企業保険管理サービス」や、保険以外にも企業経営に役立つコンサルタントや専門家などと構築した独自ネットワークサービスも準備しています。</p>
	<p>保険を中心としながら、お客様に提案できる商品を多数準備していますので、お客様に大きくお役立ちできる環境があります。</p>
					
					</div>
				</div>
				<div class="pageAboutCompanyUl width720 mb50" data-aos="fade-up">
					<ul>
						<li>勤務地</li>
						<li>倉敷市</li>
					</ul>
					<ul>
						<li>給与</li>
						<li>20～50万円</li>
					</ul>
					<ul>
						<li>雇用形態</li>
						<li>正社員</li>
					</ul>
					<ul>
						<li>求める人材</li>
						<li>営業経験のある方を歓迎します。</li>
					</ul>
					<ul>
						<li>勤務時間</li>
						<li>・09:00~18:00(休憩時間60分)<br>・10:00~19:00(休憩時間60分)<br>※時間外　月平均10時間あります。</li>
					</ul>
					<ul>
						<li>待遇・福利厚生</li>
						<li>各種社会保険完備、退職金制度、車通勤可(通勤手当上限5,000円まで)、年間休日120日以上</li>
					</ul>
				</div>
			</div>
			<div class="detail2 mb80">
				<div class="width720">
					<h4 class="h4 mb10 titleBd text-center inlineBock mb30">ルート営業スタッフ(未経験者歓迎)</h4>
					<div class="mb30">
					
<p>倉敷市、総社市、岡山市を中心に法人・個人のお客様に対する営業スタッフの募集です。</p>

<p>当社の営業スタイルは 「保険の押し売り」ではなく、保険サービスを通して、お客様の経営パートナーとなることを目指しています。保険業界や営業の経験は不要です。</p>
<p> 「人と話すことが好き」　「ずっとデスクワークするより、自分のペースで働きたい」 そんな方に向いているお仕事です。</p>

<p>自分のお客様に対し、必要な時期に訪問して契約内容の見直しや更新手続きをおこないます。新規営業や契約獲得などのノルマはありません。自分の担当顧客へのサービス提供やフォローが主な業務です。</p>					
					</div>
				</div>
				<div class="pageAboutCompanyUl width720 mb50" data-aos="fade-up">
					<ul>
						<li>勤務地</li>
						<li>倉敷市</li>
					</ul>
					<ul>
						<li>給与</li>
						<li>20～45万円</li>
					</ul>
					<ul>
						<li>雇用形態</li>
						<li>正社員</li>
					</ul>
					<ul>
						<li>求める人材</li>
						<li>経験は不問です。(未経験者歓迎)<br>※入社後、保険の知識などを学ぶ制度があります。先輩社員も丁寧に指導しますので安心してください。</li>
					</ul>
					<ul>
						<li>勤務時間</li>
						<li>・09:00~18:00(休憩時間60分)<br>・10:00~19:00(休憩時間60分)<br>※時間外　月平均10時間あり。</li>
					</ul>
					<ul>
						<li>待遇・福利厚生</li>
						<li>各種社会保険完備、昇給・賞与あり、退職金制度、車通勤可(通勤手当上限5,000円まで)、年間休日120日以上</li>
					</ul>
				</div>
				<a href="<?php echo home_url();?>/contact" class="button bold mainColor tra text-center">応募する</a>
			</div>
			<div class="detail3">
				<div class="width720">
					<h4 class="h4 mb10 titleBd text-center inlineBock mb30">事務スタッフ</h4>
					<div class="mb30">
					
<p>倉敷市、総社市、岡山市を中心にお客様に寄り添った保険サービスの提供を心がけている保険会社です。</p>
<p>保険の専門知識は全く必要ありません。まずは簡単なパソコン入力業務や電話対応、来客対応をお願いします。</p>
<p>徐々に慣れてきたら、先輩社員の指示のもとに、お客様に提出する資料作成の補助業務なども行っていただきます。</p>
<p>業務も専属の先輩スタッフが丁寧に指導しますので、ご安心ください。</p>

					</div>
				</div>
				<div class="pageAboutCompanyUl width720 mb50" data-aos="fade-up">
					<ul>
						<li>勤務地</li>
						<li>倉敷市</li>
					</ul>
					<ul>
						<li>時給</li>
						<li>時給810円～1,000円</li>
					</ul>
					<ul>
						<li>雇用形態</li>
						<li>アルバイト・パート</li>
					</ul>
					<ul>
						<li>求める人材</li>
						<li>＊高卒以上<br>＊保険業界の経験不問<br>＊パソコン（ワード・エクセル）の基本操作</li>
					</ul>
					<ul>
						<li>勤務時間</li>
						<li>１日４時間以上、週３日以上の出勤から相談に応じます</li>
					</ul>
					<ul>
						<li>待遇・福利厚生</li>
						<li>車通勤可（通勤手当上限5,000円）、各種社会保険完備（労働条件により各種保険に加入していただきます）</li>
					</ul>
				</div>
				<a href="<?php echo home_url();?>/contact" class="button bold mainColor tra text-center">応募する</a>
			</div>
		</div>
		
	</div>
</section>




<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>