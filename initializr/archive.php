<?php get_header(); ?>
<main>

<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>


<section class="under_fv" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/service_fv.png');">
	<div class="under_fv_txtarea">
		<h2 class="under_fv_jp h_mincho"><?php echo get_the_archive_title(); ?></h2>
		<p class="under_fv_eng">News</p>
	</div>
</section>





<section class="pd-common relative paperBgUnder" style="background-color: #eeede6;">
	<div class="container">
		<?php //get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>

</main>


<?php get_footer(); ?>