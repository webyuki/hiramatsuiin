<?php get_header(); ?>

<main>
	
<section class="under_fv" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/about_fv.png');">
	<div class="under_fv_txtarea">
		<h2 class="under_fv_jp h_mincho">ご挨拶</h2>
		<p class="under_fv_eng">Greeting</p>
	</div>
</section>

<section class="pd-common" style="background-color: #eeede6;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs"><p class="pt_title_eng">Greeting</p></div>
				<div class="text-center text-center-xs"><h3 class="pt_title_jp h_mincho">患者さまと向き合い、心の通う診察を</h3></div>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6 col-sm-push-6">
				<img class="mb-xs-20" src="<?php echo get_template_directory_uri(); ?>/img/about01.jpg" alt="誠実な医療をモットーに">
			</div>
			<div class="col-sm-6 col-sm-pull-6">

<p>皮膚科・泌尿器科として開業した祖父の代から100年に渡り、平松医院は地域の皆さまの「かかりつけ医」として、職場や外出先での「困ったときのより所」として、患者さまとの絆を深めてまいりました。</p>

<p>外科医としてこの医院を継いだ父は、患者さまに慕われ、誠実に治療に向き合ってきました。そんな父の姿を見て、私もいつかはここで患者さまと向き合いたいと考えていました。</p>

<p>現在、呼吸器科・内科・泌尿器科・皮膚科の疾患を診察する平松医院では、患者さまの体の”内なる声”に耳をすませ、「患者さまにとっての最善は何か？」を考えて治療にあたっています。</p>

<p>最新の研究によって得られた知識と実証をもとに、病気の早期発見・早期治療、そして予防医療に努め、ていねいな診察と説明で皆さまのお役に立つこと。</p>

<p>つらい症状だけでなく、ご家庭やお仕事、すべてを含めた患者さまのより良い治療をサポートすること。</p>

<p>これらを指針として、平松医院はこれからも「町の頼れるお医者さん」であり続けます。</p>

            </div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6">
				<img class="mb-xs-20" src="<?php echo get_template_directory_uri(); ?>/img/about02.jpg" alt="平松医院 院長">
			</div>
			<div class="col-sm-6">
				<p class="about_katagaki h_mincho">平松医院 院長 （三代目） </p>
				<p class="about_name h_mincho">平松 順一</p>
				<p>1962 年(昭和 37 年)生<br>愛媛大学医学部卒<br>岡山大学医学部第二内科勤務<br>岡山県健康づくり財団附属病院内科勤務<br>岡山大学医学博士号取得</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
			  <div class="about_ul_area">
					<ul class="ul-2 ul-xs-1 about_ul">
						<li>
							<h4 class="about_ul_title h_mincho">所属学会</h4>
							<p class="about_ul_txt">日本内科学会</p>
							<p class="about_ul_txt">日本結核病学会</p>
							<p class="about_ul_txt">日本アレルギー学会</p>
							<p class="about_ul_txt">日本老年医学会</p>
						</li>
						<li>
							<h4 class="about_ul_title h_mincho">資格</h4>
							<p class="about_ul_txt">感染管理医</p>
							<p class="about_ul_txt">日本医師会認定産業医</p>
							<p class="about_ul_txt">労働衛生コンサルタント</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="pd-common" style="background-color: #cbdecb;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs"><p class="pt_title_eng">History</p></div>
				<div class="text-center text-center-xs mb100 mb-xs-60"><h3 class="pt_title_jp h_mincho">沿革</h3></div>
				<ul class="ul-2 ul-xs-1 feature_history_ul">
					<li>
						<p class="feature_history_date h_mincho">大正12年 <span>平松醫院を開業</span></p>
						<img class="feature_history_img" src="<?php echo get_template_directory_uri(); ?>/img/feature_history01.jpg" alt="平松医院を開業">
						<p>祖父、故平松直が北区磨屋町の旅館跡に「平松醫院」皮膚泌尿器科を開業</p>
					</li>
					<li>
						<p class="feature_history_date h_mincho">大正14年 <span>現所在地に移転</span></p>
					</li>
				</ul>
				<ul class="ul-2 ul-xs-1 feature_history_ul">
					<li>
						<p class="feature_history_date h_mincho">昭和20年 <span>医院全焼</span></p>
						<p>岡山空襲の戦災により医院全焼。<br>現所在地に仮診療所を開設</p>
					</li>
					<li>
						<p class="feature_history_date h_mincho">昭和27年 <span>肛門科を併設</span></p>
						<img class="feature_history_img" src="<?php echo get_template_directory_uri(); ?>/img/feature_history02.jpg" alt="肛門科を併設">
						<p>父、平松照雄が勤務開始。<br>肛門科を併設</p>
					</li>
				</ul>
				<ul class="ul-2 ul-xs-1 feature_history_ul">
					<li>
						<p class="feature_history_date h_mincho">昭和34年 <span>全面改修</span></p>
						<img class="feature_history_img feature_history_img03" src="<?php echo get_template_directory_uri(); ?>/img/feature_history03.jpg" alt="全面改修">
						<p>旧施設を解体し、現在の「平松醫院」に全面改修</p>
					</li>
					<li>
						<p class="feature_history_date h_mincho">昭和39年 <span>２代目院長が継承</span></p>
						<p>２代目院長、平松照雄が継承</p>
					</li>
				</ul>
				<ul class="ul-2 ul-xs-1 feature_history_ul">
					<li>
						<p class="feature_history_date h_mincho">平成15年 <span>内科・呼吸器科を併設</span></p>
						<p>平松順一が勤務開始<br>内科・呼吸器科を併設</p>
					</li>
					<li>
						<p class="feature_history_date h_mincho">平成27年 <span>３代目院長が継承</span></p>
						<img class="feature_history_img feature_history_img03" src="<?php echo get_template_directory_uri(); ?>/img/feature_history04.png" alt="全面改修" style="max-width:200px;">
						<p>３代目院長、平松順一が継承<br>現在に至る</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>

</main>







<?php get_footer(); ?>