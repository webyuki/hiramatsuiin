<?php get_header(); ?>
<main style="background-color: #ebeae3;">

<section class="under_fv" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_fv.png');">
	<div class="under_fv_txtarea">
		<h2 class="under_fv_jp h_mincho">当院の特徴</h2>
		<p class="under_fv_eng">Feature</p>
	</div>
</section>


<section class="pd-common mt70 mt-sm-40 mt-xs-0 mb60 mb-sm-0 mb-xs-0 service_content feature_content feature_content01" id="" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature01.png');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 service_ul mb0 mb-xs-0">
					<li>
						<img class="visible-xs" src="<?php echo get_template_directory_uri(); ?>/img/feature01.png" alt="院内処方">
					</li>
					<li>
						<div class="service_title_outer">
							<h3 class="service_title_jp main_color h_mincho">院内処方</h3>
							<p class="service_title_eng main_color">Internal Medicine</p>
						</div>
						<div class="text-justify service_txt">
<p>お薬を院内の薬局で調剤し、直接お持ち帰りいただくことができるのが院内処方です。</p>

<p>院内処方には、<br>
・調剤にかかる費用が抑えられる<br>
・病歴を把握した上での処方なので、服薬指導が的確<br>
・調剤薬局まで足を運ぶ必要がなく、お会計も一度で済む<br>
などのメリットがあります。</p>

<p>当医院では医療上の安全を確保し、丁寧な服薬指導とともに、院内処方を行っています。</p>

<p>扱いのないお薬、または薬局で受け取りをご希望の方には、院外処方にて対応しています。お気軽にご相談下さい。</p>

<p>尚、院外処方箋の有効期限は発行日を含め4日以内です。</p>


                        </div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common mb60 mb-sm-0 mb-xs-0 service_content service_content02 feature_content feature_content02" id="" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature02.png');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 service_ul feature_service_ul mb0 mb-xs-0">
					<li>
						<div class="service_title_outer">
							<h3 class="service_title_jp main_color h_mincho">ジェネリック医薬品</h3>
							<p class="service_title_eng main_color">Generic Drug</p>
						</div>
						<div class="text-justify service_txt">
                        
<p>ジェネリック医薬品とは、最初に開発された薬の特許期間が満了した後で発売される後発医薬品で、先発品と同様の効き目が証明され、厚生労働省の承認を得ています。開発に費用がかかる先発品と比べると安価であるため、患者さまの経済的負担が軽減されるメリットがあります。</p>

<p>ただ代わりとなるジェネリック医薬品がない場合や、効きめと安全性を考慮し、先発品を使用することもあります。</p>

<p>「長期の投薬が必要となり、経済的な不安を感じる」など、お薬についての気がかりがあれば、いつでもご相談下さい。</p>
                        
                        </div>
					</li>
					<li>
						<img class="visible-xs" src="<?php echo get_template_directory_uri(); ?>/img/feature02.png" alt="ジェネリック医薬品">
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>


<section class="pd-common mt70 mt-sm-40 mt-xs-0 mb60 mb-sm-0 mb-xs-0 service_content feature_content feature_content01" id="" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature03.png');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 service_ul mb0 mb-xs-0">
					<li>
						<img class="visible-xs" src="<?php echo get_template_directory_uri(); ?>/img/feature03.png" alt="院内処方">
					</li>
					<li>
						<div class="service_title_outer">
							<h3 class="service_title_jp main_color h_mincho">多岐に渡る診療分野</h3>
							<p class="service_title_eng main_color">Various medical treatment fields</p>
						</div>
						<div class="text-justify service_txt">


<p>当院では、呼吸器科、内科、皮膚科、泌尿器科と幅広い診療科を設けています。</p>

<p>クリニックとしては珍しく、複数の診療科を設置しているのは、呼吸器科や内科で診ることの多い喘息と花粉症、皮膚科で診ることの多いアトピーを一連のものと考えているため。</p>

<p>実際、花粉症の患者さまの1/3程度は喘息へ移行する可能性を持つ方がおられ、喘息の患者さまのなかには、アトピーや花粉症を併発する方がおられます。</p>

<p>そのため当院では、皮膚症状と呼吸器症状のある病態を、ひとつのアレルギー疾患として治療しています。</p>

<p>「これは聞いてもいいのかな？」「他の科の症状だから言わなくてもいいか？」と思われても、体の中で起こった症状は関連していることが多く診断や治療の大きな手がかりになります。</p>

<p>当院では、内科、呼吸器科、泌尿器科、皮膚科のさまざまな症状を持つ患者さまの総合的な治療に力を注いでいます。是非、ご相談ください。</p>

                        </div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common mb60 mb-sm-0 mb-xs-0 service_content service_content02 feature_content feature_content02" id="" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature04.png');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 service_ul feature_service_ul mb0 mb-xs-0">
					<li>
						<div class="service_title_outer">
							<h3 class="service_title_jp main_color h_mincho">病診連携</h3>
							<p class="service_title_eng main_color">Cooperative Hospital</p>
						</div>
						<div class="text-justify service_txt">
                        

<p>患者さまの症状により、治療に最適と思われる下記病院と連携して症状の改善、健康状態の回復をサポートします。</p>
<p>通院のしやすさなども考慮しながら、患者様のご希望に合わせ、ご自宅や職場近くの病院や医院へのご紹介も行っています。</p>

<p class="">当院では予約日時の決定、症状にあった専門医の選択、詳細な情報共有など行い、紹介先での受診がスムーズに進み、患者さまの不安や負担をなくす事に重きを置いています。</p>


                        </div>
					</li>
					<li>
						<img class="visible-xs" src="<?php echo get_template_directory_uri(); ?>/img/feature04.png" alt="ジェネリック医薬品">
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about_ul_area about_ul_area_feature">
					<h4 class="about_ul_title h_mincho">連携病院一覧</h4>
					<table class="feature_list_table">
						<tbody>
							<tr>
								<th><p class="feature_list_name mb0">川崎医科大学総合医療センター</p></th>
								<td>
                                    <a href="https://goo.gl/maps/y5yZZGmEgrQnn3YGA" target="_blank">
                                        <p class="feature_list_add mb0">岡山市北区中山下2丁目6−1</p>
                                    </a>
                                </td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">岡山済生会総合病院</p></th>
								<td>
                                    <a href="https://goo.gl/maps/WcBTaPYbqLzKrL6A9" target="_blank">
                                        <p class="feature_list_add mb0">岡山市北区伊福町1丁目17−18</p>
                                    </a>
                                </td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">心臓病センター榊原病院</p></th>
								<td>
                                    <a href="https://goo.gl/maps/kF1srzBhmUAgadJJ8" target="_blank">
                                        <p class="feature_list_add mb0">岡山市北区中井町2丁目5−1</p>
                                    </a>
                                </td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">岡山赤十字病院</p></th>
								<td>
                                    <a href="https://goo.gl/maps/RAbiYnvA2Kb9uk4MA" target="_blank">
                                        <p class="feature_list_add mb0">岡山市北区青江2-1-1</p>
                                    </a>
                                </td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">岡山大学病院</p></th>
								<td>
                                    <a href="https://goo.gl/maps/Cjs8hr4ajSvFBNyJ7" target="_blank">
                                        <p class="feature_list_add mb0">岡山市北区鹿田町2-5-1</p>
                                    </a>
                                </td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">岡山市立市民病院</p></th>
								<td>
                                    <a href="https://goo.gl/maps/NpRdv4bhLUWu8KWa6" target="_blank">
                                        <p class="feature_list_add mb0">岡山市北区北長瀬表町3丁目20-101</p>
                                    </a>
                                </td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>	
</section>

	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="service_inspection_maintitle mb50 h_mincho">多くの患者様の声を<br class="visible-xs">頂いております。</h3>
				<ul class="ul-3 ul-xs-1 feature_voice_ul">
					<li>
						<!--<div class="feature_voice_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_voice01.png');"></div>-->
						<h4 class="feature_voice_title h_mincho">早期発見で適切な治療</h4>
						<p>風邪かと思って受診したところ、喘息も見つかりました。市販の薬が効かずに困っていたので、原因がわかってよかったです。</p>
					</li>
					<li>
						<!--<div class="feature_voice_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_voice01.png');"></div>-->
						<h4 class="feature_voice_title h_mincho">家族でお世話になってます</h4>
						<p>アレルギーの治療、水虫の治療、予防接種など幅広い治療を丁寧にしてくれます。我が家では体のことならなんでも平松先生のお世話になっています。</p>
					</li>
					<li>
						<!--<div class="feature_voice_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_voice01.png');"></div>-->
						<h4 class="feature_voice_title h_mincho">待ち時間が短くて助かりました</h4>
						<p>診察前の待ち時間も診察後の待ち時間も短かったです。また土曜日の診察もしていただけるので、仕事が忙しく時間が取りにくい中でとても助かりました。</p>
					</li>
                    <!--
					<li>
						<h4 class="feature_voice_title h_mincho">感謝に堪えません</h4>
						<p>急な症状で飛び込んだのに、丁寧に診察してくれました。おかげさまで大ごとにならずにすみました。人間的に素晴らしい先生です。</p>
					</li>
                    -->
				</ul>
			</div>
		</div>
	</div>
</section>
	

</main>






<?php get_footer(); ?>